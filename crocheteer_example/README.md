# Crocheteer - Example

- [Introduction](#introduction)
- [Purpose](#purpose)
- [Structure](#structure)
- [How To Use](#how-to-use)
- [Examples](#examples)
- [Requirements](#requirements)

## Introduction

Example module for the Crocheteer module.

## Purpose

This module is meant to act as a reference when implementing Hook Annotations in custom project-specific modules.

## Structure

All example Annotations are contained in `src/Plugin/Hook`, so make sure to check that out.

## How To Use

Three simple steps:

* Copy the desired Hook class from this module's `src/Plugin/Hook` directory
* Paste the copied Hook class in the desired module's `src/Plugin/Hook` directory
* Adjust as needed (file name & code)

## Examples

See this module's `src/Plugin/Hook` directory.

## Requirements

PHP minimal version required: **7.2**
