<?php

namespace Drupal\crocheteer_example\Plugin\crocheteer\Hook;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\crocheteer\Annotation\HookWidgetFormAlter;
use Drupal\crocheteer\Plugin\Hook\WidgetForm\Alter\HookWidgetFormAlterPlugin;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class HookWidgetFormAlterTitle.
 *
 * Only implement the ContainerFactoryPluginInterface if you need dependency
 * injection mechanisms.
 *
 * @HookWidgetFormAlter(
 *   id = "crocheteer_example_widget_form_alter_title",
 *   title = @Translation("Crocheteer Example Widget Form Alter Title"),
 *   widgetIds = {
 *     "title",
 *   },
 * )
 */
final class HookWidgetFormAlterTitle extends HookWidgetFormAlterPlugin implements ContainerFactoryPluginInterface {

  /**
   * The injected Drupal Messenger dependency.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private $messenger;

  /**
   * {@inheritdoc}
   *
   * Only add this static create method to your Plugin if you need dependency
   * injection mechanisms.
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) : HookPluginInterface {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('messenger')
    );
  }

  /**
   * HookWidgetFormAlterTitle constructor.
   *
   * Only add this constructor method to your Plugin if you need dependency
   * injection mechanisms.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $pluginId
   *   The plugin ID for the plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The Drupal Messenger.
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, MessengerInterface $messenger) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function hook() : void {
    /* @var \Drupal\Core\Field\FieldItemList $items */
    $items = $this->event->getContext()['items'];
    $this->messenger->addStatus('Hooks Example: Widget ' . $items->getName() . ' Form Altered!');
  }

}
