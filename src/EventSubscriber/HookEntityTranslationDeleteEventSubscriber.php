<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationDeleteEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Class HookEntityTranslationDeleteEventSubscriber.
 *
 * Hook Event Subscriber class for the Entity Translation Delete Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\TranslationDelete\HookEntityTranslationDeletePluginManager $pluginManager
 */
final class HookEntityTranslationDeleteEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_TRANSLATION_DELETE => 'onEntityTranslationDelete',
    ];
  }

  /**
   * On Entity Translation Delete Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationDeleteEvent $event
   *   The Entity Translation Delete Event.
   */
  public function onEntityTranslationDelete(EntityTranslationDeleteEvent $event) : void {
    $this->handleHooks($event);
  }

}
