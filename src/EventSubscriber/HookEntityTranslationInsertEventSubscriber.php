<?php

namespace Drupal\crocheteer\EventSubscriber;

use Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationInsertEvent;
use Drupal\hook_event_dispatcher\HookEventDispatcherInterface;

/**
 * Class HookEntityTranslationInsertEventSubscriber.
 *
 * Hook Event Subscriber class for the Entity Translation Insert Event.
 *
 * @property-read \Drupal\crocheteer\Plugin\Hook\Entity\TranslationInsert\HookEntityTranslationInsertPluginManager $pluginManager
 */
final class HookEntityTranslationInsertEventSubscriber extends HookEventSubscriber {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array {
    return [
      HookEventDispatcherInterface::ENTITY_TRANSLATION_INSERT => 'onEntityTranslationInsert',
    ];
  }

  /**
   * On Entity Translation Insert Event.
   *
   * @param \Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationInsertEvent $event
   *   The Entity Translation Insert Event.
   */
  public function onEntityTranslationInsert(EntityTranslationInsertEvent $event) : void {
    $this->handleHooks($event);
  }

}
