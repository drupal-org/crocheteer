<?php

namespace Drupal\crocheteer\Plugin\Hook\Js\Alter;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookJsAlterPlugin.
 *
 * Hook Js Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\JsAlterEvent $event
 */
abstract class HookJsAlterPlugin extends HookPlugin {

  /**
   * Javascript.
   *
   * @var array
   */
  protected $javascript;

  /**
   * AttachedAssets.
   *
   * @var \Drupal\Core\Asset\AttachedAssetsInterface
   */
  protected $attachedAssets;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->javascript = $this->event->getJavascript();
    $this->attachedAssets = $this->event->getAttachedAssets();
  }

}
