<?php

namespace Drupal\crocheteer\Plugin\Hook\Js;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookJsAnnotation.
 *
 * Base class for all Hook Js Annotation classes.
 */
abstract class HookJsAnnotation extends HookAnnotation {}
