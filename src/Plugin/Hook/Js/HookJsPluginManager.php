<?php

namespace Drupal\crocheteer\Plugin\Hook\Js;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookJsPluginManager.
 *
 * Base Plugin Manager class for all Hook Js Plugin classes.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EventInterface $event
 */
abstract class HookJsPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [];
  }

}
