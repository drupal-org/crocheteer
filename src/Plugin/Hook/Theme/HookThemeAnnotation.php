<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookThemeAnnotation.
 *
 * Base class for all Hook Theme Annotation classes.
 */
abstract class HookThemeAnnotation extends HookAnnotation {}
