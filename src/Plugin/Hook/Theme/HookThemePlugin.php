<?php

namespace Drupal\crocheteer\Plugin\Hook\Theme;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use RuntimeException;

/**
 * Class HookThemePlugin.
 *
 * Hook Theme Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\ThemeEvent $event
 */
abstract class HookThemePlugin extends HookPlugin {

  /**
   * Existing implementations.
   *
   * @var array
   */
  protected $existing;

  /**
   * Added themes.
   *
   * @var array
   */
  protected $newThemes;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->existing = $this->event->getExisting();
    $this->newThemes = $this->event->getNewThemes();
  }

  /**
   * Add new theme.
   *
   * @param string $theme
   *   Theme hook.
   * @param array $information
   *   Information array.
   *
   * @see \hook_theme()
   * Have a look at the return statement.
   *
   * @throws \RuntimeException
   */
  protected function addNewTheme($theme, array $information) : void {
    if (empty($information['path'])) {
      throw new RuntimeException(
        'Missing path in the information array. ThemeEvent needs the path to be set manually, to have a proper default theme implementation. See \hook_theme() for more information.'
      );
    }
    $this->newThemes[$theme] = $information;
  }

  /**
   * Add new themes.
   *
   * @param array $themes
   *   The new theme information.
   *
   * @see \hook_theme()
   * Have a look at the return statement.
   *
   * @throws \RuntimeException
   */
  protected function addNewThemes(array $themes) : void {
    foreach ($themes as $theme => $information) {
      $this->addNewTheme($theme, $information);
    }
  }

}
