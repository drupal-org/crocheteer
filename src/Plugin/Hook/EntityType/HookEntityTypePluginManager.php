<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityType;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookEntityTypePluginManager.
 *
 * Base Plugin Manager class for all Hook EntityType Plugin classes.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EventInterface $event
 */
abstract class HookEntityTypePluginManager extends HookPluginManager {}
