<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityType\BaseFieldInfo;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityBaseFieldInfoPlugin.
 *
 * Hook Entity Base Field Info Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityType\EntityBaseFieldInfoEvent $event
 */
abstract class HookEntityBaseFieldInfoPlugin extends HookPlugin {

  /**
   * The entity type.
   *
   * @var \Drupal\Core\Entity\EntityTypeInterface
   */
  protected $entityType;

  /**
   * The fields.
   *
   * @var static[]
   */
  protected $fields = [];

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->entityType = $this->event->getEntityType();
    $this->fields = $this->event->getFields();
  }

}
