<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\Load;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityLoadPlugin.
 *
 * Hook Entity Load Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityLoadEvent $event
 */
abstract class HookEntityLoadPlugin extends HookEntityPlugin {

  /**
   * The entities.
   *
   * @var array
   */
  protected $entities;

  /**
   * The entity type id.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->entities = $this->event->getEntities();
    $this->entityTypeId = $this->event->getEntityTypeId();
  }

}
