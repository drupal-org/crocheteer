<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\View;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityViewPlugin.
 *
 * Hook Entity View Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityViewEvent $event
 */
abstract class HookEntityViewPlugin extends HookEntityPlugin {

  /**
   * A renderable array representing the entity content.
   *
   * @var array
   */
  protected $build;
  /**
   * The entity view display.
   *
   * @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface
   */
  protected $display;
  /**
   * The view mode.
   *
   * @var string
   */
  protected $viewMode;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->build = $this->event->getBuild();
    $this->display = $this->event->getDisplay();
    $this->viewMode = $this->event->getViewMode();
  }

}
