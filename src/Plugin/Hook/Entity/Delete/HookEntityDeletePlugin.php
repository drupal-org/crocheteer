<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\Delete;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;

/**
 * Class HookEntityDeletePlugin.
 *
 * Hook Entity Delete Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityDeleteEvent $event
 */
abstract class HookEntityDeletePlugin extends HookEntityPlugin {}
