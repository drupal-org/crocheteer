<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\Access;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityAccessPlugin.
 *
 * Hook Entity Access Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityAccessEvent $event
 */
abstract class HookEntityAccessPlugin extends HookEntityPlugin {

  /**
   * The operation that is to be performed on $entity.
   *
   * @var string
   */
  protected $operation;

  /**
   * The account trying to access the entity.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The access result.
   *
   * @var \Drupal\Core\Access\AccessResultInterface
   */
  protected $accessResult;

  /**
   * {@inheritdoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->operation = $this->event->getOperation();
    $this->account = $this->event->getAccount();
    $this->accessResult = $this->event->getAccessResult();
  }

}
