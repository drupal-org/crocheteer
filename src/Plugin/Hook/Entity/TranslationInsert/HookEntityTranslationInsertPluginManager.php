<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\TranslationInsert;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookEntityPreSave;
use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPluginManager;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Class HookEntityTranslationInsertPluginManager.
 *
 * @see \Drupal\crocheteer\Annotation\HookEntityTranslationInsert
 * @see \Drupal\crocheteer\Plugin\Hook\HookPluginInterface
 * @see plugin_api
 */
final class HookEntityTranslationInsertPluginManager extends HookEntityPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookEntityPreSave::class,
      'crocheteer_entity_translation_insert'
    );
  }

}
