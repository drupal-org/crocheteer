<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\TranslationInsert;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;

/**
 * Class HookEntityTranslationInsertPlugin.
 *
 * Hook Entity Translation Insert Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationInsertEvent $event
 */
abstract class HookEntityTranslationInsertPlugin extends HookEntityPlugin {}
