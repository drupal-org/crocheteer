<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\Update;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityUpdatePlugin.
 *
 * Hook Entity Update Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityUpdateEvent $event
 */
abstract class HookEntityUpdatePlugin extends HookEntityPlugin {

  /**
   * The original Entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected $original;

  /**
   * {@inheritdoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->original = $this->event->getOriginalEntity();
  }

}
