<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\TranslationDelete;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;

/**
 * Class HookEntityTranslationDeletePlugin.
 *
 * Hook Entity Translation Delete Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityTranslationDeleteEvent $event
 */
abstract class HookEntityTranslationDeletePlugin extends HookEntityPlugin {}
