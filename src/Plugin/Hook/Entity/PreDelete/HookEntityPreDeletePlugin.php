<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\PreDelete;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;

/**
 * Class HookEntityPreDeletePlugin.
 *
 * Hook Entity Pre-Delete Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityPredeleteEvent $event
 */
abstract class HookEntityPreDeletePlugin extends HookEntityPlugin {}
