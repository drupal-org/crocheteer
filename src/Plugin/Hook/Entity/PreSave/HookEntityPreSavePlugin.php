<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\PreSave;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityPreSavePlugin.
 *
 * Hook Entity Pre-Save Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityPreSaveEvent $event
 */
abstract class HookEntityPreSavePlugin extends HookEntityPlugin {

  /**
   * Create Operation constant.
   *
   * @var string
   */
  protected const OPERATION_CREATE = 'create';

  /**
   * Update Operation constant.
   *
   * @var string
   */
  protected const OPERATION_UPDATE = 'update';

  /**
   * The original Entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  protected $original;

  /**
   * The Pre-Save operation being performed.
   *
   * @var string
   */
  protected $operation;

  /**
   * {@inheritdoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->original = $this->event->getOriginalEntity();
    $this->operation = $this->original === NULL ? self::OPERATION_CREATE : self::OPERATION_UPDATE;
  }

}
