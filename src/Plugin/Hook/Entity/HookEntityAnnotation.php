<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookEntityAnnotation.
 *
 * Base class for all Hook Entity Annotation classes.
 */
abstract class HookEntityAnnotation extends HookAnnotation {

  /**
   * Array of relevant Entity Type IDs.
   *
   * @var string[]
   */
  public $entityTypeIds;

  /**
   * Array of relevant bundles.
   *
   * @var string[]
   */
  public $bundles;

}
