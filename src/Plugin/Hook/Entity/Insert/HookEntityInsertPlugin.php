<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\Insert;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;

/**
 * Class HookEntityInsertPlugin.
 *
 * Hook Entity Insert Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityInsertEvent $event
 */
abstract class HookEntityInsertPlugin extends HookEntityPlugin {}
