<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\Create;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookEntityCreate;
use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPluginManager;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Class HookEntityCreatePluginManager.
 *
 * @see \Drupal\crocheteer\Annotation\HookEntityCreate
 * @see \Drupal\crocheteer\Plugin\Hook\HookPluginInterface
 * @see plugin_api
 */
final class HookEntityCreatePluginManager extends HookEntityPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookEntityCreate::class,
      'crocheteer_entity_create'
    );
  }

}
