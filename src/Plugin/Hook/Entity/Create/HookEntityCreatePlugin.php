<?php

namespace Drupal\crocheteer\Plugin\Hook\Entity\Create;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityPlugin;

/**
 * Class HookEntityCreatePlugin.
 *
 * Hook Entity Create Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Entity\EntityCreateEvent $event
 */
abstract class HookEntityCreatePlugin extends HookEntityPlugin {}
