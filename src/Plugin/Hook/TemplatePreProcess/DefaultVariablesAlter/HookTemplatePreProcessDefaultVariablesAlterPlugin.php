<?php

namespace Drupal\crocheteer\Plugin\Hook\TemplatePreProcess\DefaultVariablesAlter;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookTemplatePreProcessDefaultVariablesAlterPlugin.
 *
 * Hook Template Pre-Process Default Variables Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\TemplatePreProcessDefaultVariablesAlterEvent $event
 */
abstract class HookTemplatePreProcessDefaultVariablesAlterPlugin extends HookPlugin {

  /**
   * Default template variables.
   *
   * @var array
   */
  protected $variables;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->variables = $this->event->getVariables();
  }

}
