<?php

namespace Drupal\crocheteer\Plugin\Hook\TemplatePreProcess;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookTemplatePreProcessAnnotation.
 *
 * Base class for all Hook Template Pre-Process Annotation classes.
 */
abstract class HookTemplatePreProcessAnnotation extends HookAnnotation {}
