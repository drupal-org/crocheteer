<?php

namespace Drupal\crocheteer\Plugin\Hook\Page\Bottom;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookPageBottomPlugin.
 *
 * Hook Page Bottom Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Page\PageBottomEvent $event
 */
abstract class HookPageBottomPlugin extends HookPlugin {

  /**
   * The build array.
   *
   * @var array
   */
  protected $build;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->build = $this->event->getBuild();
  }

}
