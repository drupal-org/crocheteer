<?php

namespace Drupal\crocheteer\Plugin\Hook\Page;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookPageAnnotation.
 *
 * Base class for all Hook Page Annotation classes.
 */
abstract class HookPageAnnotation extends HookAnnotation {}
