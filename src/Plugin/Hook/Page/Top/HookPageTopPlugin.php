<?php

namespace Drupal\crocheteer\Plugin\Hook\Page\Top;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookPageTopPlugin.
 *
 * Hook Page Top Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Page\PageTopEvent $event
 */
abstract class HookPageTopPlugin extends HookPlugin {

  /**
   * The build array.
   *
   * @var array
   */
  protected $build;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->build = $this->event->getBuild();
  }

}
