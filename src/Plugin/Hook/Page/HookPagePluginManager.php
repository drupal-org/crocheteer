<?php

namespace Drupal\crocheteer\Plugin\Hook\Page;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookPagePluginManager.
 *
 * Base Plugin Manager class for all Hook Page Plugin classes.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EventInterface $event
 */
abstract class HookPagePluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [];
  }

}
