<?php

namespace Drupal\crocheteer\Plugin\Hook\Path\Insert;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Class HookPathInsertPlugin.
 *
 * Hook Path Insert Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\PathInsertEvent $event
 */
abstract class HookPathInsertPlugin extends HookPlugin {}
