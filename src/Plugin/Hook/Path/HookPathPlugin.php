<?php

namespace Drupal\crocheteer\Plugin\Hook\Path;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookPathPlugin.
 *
 * Hook Path Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\BasePathEvent $event
 */
abstract class HookPathPlugin extends HookPlugin {

  /**
   * The source like '/node/1'.
   *
   * @var string
   */
  protected $source;

  /**
   * The alias for the source.
   *
   * @var string
   */
  protected $alias;

  /**
   * Lang code.
   *
   * @var string
   */
  protected $langcode;

  /**
   * The path id.
   *
   * @var int
   */
  protected $pid;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->source = $this->event->getSource();
    $this->alias = $this->event->getAlias();
    $this->langcode = $this->event->getLangcode();
    $this->pid = $this->event->getPid();
  }

}
