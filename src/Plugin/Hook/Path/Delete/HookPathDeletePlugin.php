<?php

namespace Drupal\crocheteer\Plugin\Hook\Path\Delete;

use Drupal\crocheteer\Plugin\Hook\Path\HookPathPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookPathDeletePlugin.
 *
 * Hook Path Delete Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\PathDeleteEvent $event
 */
abstract class HookPathDeletePlugin extends HookPathPlugin {

  /**
   * The redirect.
   *
   * @var bool
   */
  protected $redirect;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->redirect = $this->event->isRedirect();
  }

}
