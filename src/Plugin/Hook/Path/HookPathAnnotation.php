<?php

namespace Drupal\crocheteer\Plugin\Hook\Path;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookPathAnnotation.
 *
 * Base class for all Hook Path Annotation classes.
 */
abstract class HookPathAnnotation extends HookAnnotation {}
