<?php

namespace Drupal\crocheteer\Plugin\Hook\Path\Update;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;

/**
 * Class HookPathUpdatePlugin.
 *
 * Hook Path Update Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Path\PathUpdateEvent $event
 */
abstract class HookPathUpdatePlugin extends HookPlugin {}
