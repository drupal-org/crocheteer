<?php

namespace Drupal\crocheteer\Plugin\Hook\ThemeSuggestions\Alter;

use Drupal\crocheteer\Plugin\Hook\ThemeSuggestions\HookThemeSuggestionsPlugin;

/**
 * Class HookThemeSuggestionsAlterPlugin.
 *
 * Hook Theme Suggestions Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\ThemeSuggestionsAlterEvent $event
 */
abstract class HookThemeSuggestionsAlterPlugin extends HookThemeSuggestionsPlugin {}
