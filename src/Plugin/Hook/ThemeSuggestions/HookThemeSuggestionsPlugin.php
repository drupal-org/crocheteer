<?php

namespace Drupal\crocheteer\Plugin\Hook\ThemeSuggestions;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookThemeSuggestionsPlugin.
 *
 * Hook Theme Suggestions Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Theme\BaseThemeSuggestionsEvent $event
 */
abstract class HookThemeSuggestionsPlugin extends HookPlugin {

  /**
   * Array of suggestions.
   *
   * @var array
   */
  protected $suggestions;

  /**
   * Variables.
   *
   * @var array
   */
  protected $variables;

  /**
   * Hook name.
   *
   * @var string
   */
  protected $hook;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->suggestions = $this->event->getSuggestions();
    $this->variables = $this->event->getVariables();
    $this->hook = $this->event->getHook();
  }

}
