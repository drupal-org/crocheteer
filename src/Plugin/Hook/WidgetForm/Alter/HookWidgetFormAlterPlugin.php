<?php

namespace Drupal\crocheteer\Plugin\Hook\WidgetForm\Alter;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookWidgetFormAlterPlugin.
 *
 * Hook Widget Form Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Form\WidgetFormAlterEvent $event
 */
abstract class HookWidgetFormAlterPlugin extends HookPlugin {

  /**
   * The field widget form element.
   *
   * @var array
   */
  protected $element;

  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * The context.
   *
   * @var array
   */
  protected $context;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->element = $this->event->getElement();
    $this->formState = $this->event->getFormState();
    $this->context = $this->event->getContext();
  }

}
