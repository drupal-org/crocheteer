<?php

namespace Drupal\crocheteer\Plugin\Hook\WidgetForm;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookWidgetFormPluginManager.
 *
 * Base Plugin Manager class for all Hook Widget Form Plugin classes.
 */
abstract class HookWidgetFormPluginManager extends HookPluginManager {}
