<?php

namespace Drupal\crocheteer\Plugin\Hook\Toolbar\Alter;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookToolbarAlterPlugin.
 *
 * Hook Toolbar Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Toolbar\ToolbarAlterEvent $event
 */
abstract class HookToolbarAlterPlugin extends HookPlugin {

  /**
   * The toolbar items.
   *
   * @var array
   */
  protected $items;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->items = $this->event->getItems();
  }

}
