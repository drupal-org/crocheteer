<?php

namespace Drupal\crocheteer\Plugin\Hook\Toolbar;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookToolbarPluginManager.
 *
 * Base Plugin Manager class for all Hook Toolbar Plugin classes.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EventInterface $event
 */
abstract class HookToolbarPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [];
  }

}
