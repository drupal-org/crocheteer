<?php

namespace Drupal\crocheteer\Plugin\Hook\Toolbar;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookToolbarAnnotation.
 *
 * Base class for all Hook Toolbar Annotation classes.
 */
abstract class HookToolbarAnnotation extends HookAnnotation {}
