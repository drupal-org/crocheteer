<?php

namespace Drupal\crocheteer\Plugin\Hook\Cron;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookCronAnnotation.
 *
 * Base class for all Hook Cron Annotation classes.
 */
abstract class HookCronAnnotation extends HookAnnotation {}
