<?php

namespace Drupal\crocheteer\Plugin\Hook\User\Logout;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookUserLogoutPlugin.
 *
 * Hook User Logout Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserLogoutEvent $event
 */
abstract class HookUserLogoutPlugin extends HookPlugin {

  /**
   * Account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->account = $this->event->getAccount();
  }

}
