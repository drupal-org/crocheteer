<?php

namespace Drupal\crocheteer\Plugin\Hook\User;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookUserPluginManager.
 *
 * Base Plugin Manager class for all Hook User Plugin classes.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EventInterface $event
 */
abstract class HookUserPluginManager extends HookPluginManager {}
