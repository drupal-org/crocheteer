<?php

namespace Drupal\crocheteer\Plugin\Hook\User\Login;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookUserLoginPlugin.
 *
 * Hook User Login Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserLoginEvent $event
 */
abstract class HookUserLoginPlugin extends HookPlugin {

  /**
   * Account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->account = $this->event->getAccount();
  }

}
