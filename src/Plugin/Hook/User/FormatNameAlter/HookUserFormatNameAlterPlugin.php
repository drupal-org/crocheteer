<?php

namespace Drupal\crocheteer\Plugin\Hook\User\FormatNameAlter;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookUserFormatNameAlterPlugin.
 *
 * Hook User FormatNameAlter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\User\UserFormatNameAlterEvent $event
 */
abstract class HookUserFormatNameAlterPlugin extends HookPlugin {

  /**
   * Name.
   *
   * @var string
   */
  protected $name;

  /**
   * Account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->name = $this->event->getName();
    $this->account = $this->event->getAccount();
  }

}
