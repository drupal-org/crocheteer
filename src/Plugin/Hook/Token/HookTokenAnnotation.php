<?php

namespace Drupal\crocheteer\Plugin\Hook\Token;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookTokenAnnotation.
 *
 * Base class for all Hook Token Annotation classes.
 */
abstract class HookTokenAnnotation extends HookAnnotation {}
