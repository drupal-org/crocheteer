<?php

namespace Drupal\crocheteer\Plugin\Hook\Token;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookTokenPluginManager.
 *
 * Base Plugin Manager class for all Hook Token Plugin classes.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EventInterface $event
 */
abstract class HookTokenPluginManager extends HookPluginManager {

  /**
   * {@inheritdoc}
   */
  protected function getRelevancyProperties() : array {
    return [];
  }

}
