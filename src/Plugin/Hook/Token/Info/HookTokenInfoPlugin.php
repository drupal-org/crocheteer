<?php

namespace Drupal\crocheteer\Plugin\Hook\Token\Info;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use Drupal\hook_event_dispatcher\Value\Token;
use Drupal\hook_event_dispatcher\Value\TokenType;

/**
 * Class HookTokenInfoPlugin.
 *
 * Hook Token Info Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Token\TokensInfoEvent $event
 */
abstract class HookTokenInfoPlugin extends HookPlugin {

  /**
   * Token types.
   *
   * An associative array of token types (groups). Each token type is
   * an associative array with the following components:
   * - name: The translated human-readable short name of the token type.
   * - description (optional): A translated longer description of the token
   *   type.
   * - needs-data: The type of data that must be provided to
   *   \Drupal\Core\Utility\Token::replace() in the $data argument (i.e., the
   *   key name in $data) in order for tokens of this type to be used in the
   *   $text being processed. For instance, if the token needs a node object,
   *   'needs-data' should be 'node', and to use this token in
   *   \Drupal\Core\Utility\Token::replace(), the caller needs to supply a
   *   node object as $data['node']. Some token data can also be supplied
   *   indirectly; for instance, a node object in $data supplies a user object
   *   (the author of the node), allowing user tokens to be used when only
   *   a node data object is supplied.
   *
   * @var array
   */
  protected $tokenTypes = [];

  /**
   * Tokens.
   *
   * An associative array of tokens. The outer array is keyed by the
   * group name (the same key as in the types array). Within each group of
   * tokens, each token item is keyed by the machine name of the token, and
   * each token item has the following components:
   * - name: The translated human-readable short name of the token.
   * - description (optional): A translated longer description of the token.
   * - type (optional): A 'needs-data' data type supplied by this token, which
   *   should match a 'needs-data' value from another token type. For example,
   *   the node author token provides a user object, which can then be used
   *   for token replacement data in \Drupal\Core\Utility\Token::replace()
   *   without having to supply a separate user object.
   *
   * @var array
   */
  protected $tokens = [];

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->tokenTypes = $this->event->getTokenTypes();
    $this->tokens = $this->event->getTokens();
  }

  /**
   * Add token type.
   *
   * @param \Drupal\hook_event_dispatcher\Value\TokenType $type
   *   The token type.
   */
  protected function addTokenType(TokenType $type) : void {
    $this->tokenTypes[$type->getType()] = [
      'name' => $type->getName(),
      'description' => $type->getDescription(),
      'needs-data' => $type->getNeedsData(),
    ];
  }

  /**
   * Add token.
   *
   * @param \Drupal\hook_event_dispatcher\Value\Token $type
   *   The token.
   */
  protected function addToken(Token $type) : void {
    $this->tokens[$type->getType()][$type->getToken()] = [
      'name' => $type->getName(),
      'description' => $type->getDescription(),
    ];
  }

}
