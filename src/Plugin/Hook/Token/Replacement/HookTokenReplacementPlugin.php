<?php

namespace Drupal\crocheteer\Plugin\Hook\Token\Replacement;

use Drupal\Component\Render\MarkupInterface;
use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use UnexpectedValueException;
use function is_string;

/**
 * Class HookTokenReplacementPlugin.
 *
 * Hook Token Replacement Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Token\TokensReplacementEvent $event
 */
abstract class HookTokenReplacementPlugin extends HookPlugin {

  /**
   * An associative array of replacement values.
   *
   * Keyed by the raw [type:token] strings from the original text.
   * The returned values must be either plain
   * text strings, or an object implementing MarkupInterface if they are
   * HTML-formatted.
   *
   * @var string|\Drupal\Component\Render\MarkupInterface[]
   */
  protected $replacementValues;

  /**
   * Type.
   *
   * @var string
   */
  protected $type;

  /**
   * Tokens.
   *
   * @var array
   */
  protected $tokens;

  /**
   * Data.
   *
   * @var array
   */
  protected $data;

  /**
   * Options.
   *
   * @var array
   */
  protected $options;

  /**
   * Bubbleable meta data.
   *
   * @var \Drupal\Core\Render\BubbleableMetadata
   */
  protected $bubbleableMetadata;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->replacementValues = $this->event->getReplacementValues();
    $this->type = $this->event->getType();
    $this->tokens = $this->event->getTokens();
    $this->data = $this->event->getRawData();
    $this->options = $this->event->getOptions();
    $this->bubbleableMetadata = $this->event->getBubbleableMetadata();
  }

  /**
   * Getter for single data member.
   *
   * @param string $key
   *   The key for the additional token data, like 'node'.
   * @param mixed $default
   *   The default value, if data does not exists.
   *
   * @return mixed
   *   The value.
   */
  protected function getData($key, $default = NULL) {
    return $this->data[$key] ?? $default;
  }

  /**
   * Sets a replacement value for a token.
   *
   * @param string $type
   *   The token type like 'node'.
   * @param string $token
   *   The name of the token, like 'url'.
   * @param string|\Drupal\Component\Render\MarkupInterface $replacement
   *   The replacement value.
   *
   * @throws \UnexpectedValueException
   */
  protected function setReplacementValue($type, $token, $replacement) : void {
    if (!is_string($type)) {
      throw new UnexpectedValueException('Type should be a string');
    }
    if (!is_string($token)) {
      throw new UnexpectedValueException('Token should be a string');
    }
    if (!$this->forToken($type, $token)) {
      throw new UnexpectedValueException('Requested replacement is not requested');
    }
    if (!is_string($replacement) && !$replacement instanceof MarkupInterface) {
      throw new UnexpectedValueException('Replacement value should be a string or instanceof MarkupInterface');
    }
    $this->replacementValues["[$type:$token]"] = $replacement;
  }

  /**
   * Check if the event is for the given token.
   *
   * @param string $type
   *   The token type like 'node'.
   * @param string $token
   *   The token type like 'url'.
   *
   * @return bool
   *   TRUE if there is one.
   */
  protected function forToken($type, $token) : bool {
    return $this->type === $type && isset($this->tokens[$token]);
  }

}
