<?php

namespace Drupal\crocheteer\Plugin\Hook\Block;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookBlockPlugin.
 *
 * Hook Block Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Block\BaseBlockEvent $event
 */
abstract class HookBlockPlugin extends HookPlugin {

  /**
   * The build array.
   *
   * @var array
   */
  protected $build;

  /**
   * The block.
   *
   * @var \Drupal\Core\Block\BlockPluginInterface
   */
  protected $block;

  /**
   * {@inheritdoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->build = $this->event->getBuild();
    $this->block = $this->event->getBlock();
  }

}
