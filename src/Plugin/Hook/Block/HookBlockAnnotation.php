<?php

namespace Drupal\crocheteer\Plugin\Hook\Block;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookBlockAnnotation.
 *
 * Base class for all Hook Block Annotation classes.
 */
abstract class HookBlockAnnotation extends HookAnnotation {

  /**
   * Array of relevant Block Base IDs.
   *
   * @var string[]
   */
  public $baseIds;

}
