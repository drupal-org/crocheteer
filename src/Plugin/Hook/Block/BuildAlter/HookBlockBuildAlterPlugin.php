<?php

namespace Drupal\crocheteer\Plugin\Hook\Block\BuildAlter;

use Drupal\crocheteer\Plugin\Hook\Block\HookBlockPlugin;

/**
 * Class HookBlockBuildAlterPlugin.
 *
 * Hook Block Build Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Block\BlockBuildAlterEvent $event
 */
abstract class HookBlockBuildAlterPlugin extends HookBlockPlugin {}
