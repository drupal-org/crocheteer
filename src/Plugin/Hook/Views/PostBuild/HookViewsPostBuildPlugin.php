<?php

namespace Drupal\crocheteer\Plugin\Hook\Views\PostBuild;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsPlugin;

/**
 * Class HookViewsPostBuildPlugin.
 *
 * Hook Views Post-Build Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPostBuildEvent $event
 */
abstract class HookViewsPostBuildPlugin extends HookViewsPlugin {}
