<?php

namespace Drupal\crocheteer\Plugin\Hook\Views\PostExecute;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsPlugin;

/**
 * Class HookViewsPostExecutePlugin.
 *
 * Hook Views Post Execute Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPostExecuteEvent $event
 */
abstract class HookViewsPostExecutePlugin extends HookViewsPlugin {}
