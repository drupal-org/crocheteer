<?php

namespace Drupal\crocheteer\Plugin\Hook\Views;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookViewsPlugin.
 *
 * Hook Views Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\BaseViewsEvent $event
 */
abstract class HookViewsPlugin extends HookPlugin {

  /**
   * The view.
   *
   * @var \Drupal\views\ViewExecutable
   */
  protected $view;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->view = $this->event->getView();
  }

}
