<?php

namespace Drupal\crocheteer\Plugin\Hook\Views\PreBuild;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsPlugin;

/**
 * Class HookViewsPreBuildPlugin.
 *
 * Hook Views Pre-Build Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPreBuildEvent $event
 */
abstract class HookViewsPreBuildPlugin extends HookViewsPlugin {}
