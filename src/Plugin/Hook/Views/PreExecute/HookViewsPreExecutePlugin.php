<?php

namespace Drupal\crocheteer\Plugin\Hook\Views\PreExecute;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsPlugin;

/**
 * Class HookViewsPreExecutePlugin.
 *
 * Hook Views Pre-Execute Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsPreExecuteEvent $event
 */
abstract class HookViewsPreExecutePlugin extends HookViewsPlugin {}
