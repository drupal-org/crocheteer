<?php

namespace Drupal\crocheteer\Plugin\Hook\Views\Data;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;
use function array_merge_recursive;

/**
 * Class HookViewsDataPlugin.
 *
 * Hook Views Data Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsDataEvent $event
 */
abstract class HookViewsDataPlugin extends HookPlugin {

  /**
   * New views data.
   *
   * @var array
   */
  protected $data = [];

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->data = $this->event->getData();
  }

  /**
   * Add data to the views data.
   *
   * @param array $data
   *   Data to add to the views data.
   *
   * @see \hook_views_data()
   */
  protected function addData(array $data) : void {
    $this->data = array_merge_recursive($this->data, $data);
  }

}
