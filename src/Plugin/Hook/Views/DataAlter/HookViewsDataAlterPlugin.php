<?php

namespace Drupal\crocheteer\Plugin\Hook\Views\DataAlter;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookViewsDataAlterPlugin.
 *
 * Hook Views Data Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Views\ViewsDataAlterEvent $event
 */
abstract class HookViewsDataAlterPlugin extends HookPlugin {

  /**
   * Data.
   *
   * @var array
   */
  protected $data;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->data = $this->event->getData();
  }

}
