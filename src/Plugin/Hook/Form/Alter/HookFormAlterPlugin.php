<?php

namespace Drupal\crocheteer\Plugin\Hook\Form\Alter;

use Drupal\crocheteer\Plugin\Hook\Form\HookFormPlugin;

/**
 * Class HookFormAlterPlugin.
 *
 * Hook Form Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Form\FormAlterEvent $event
 */
abstract class HookFormAlterPlugin extends HookFormPlugin {}
