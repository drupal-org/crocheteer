<?php

namespace Drupal\crocheteer\Plugin\Hook\Form\Alter;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\crocheteer\Annotation\HookFormAlter;
use Drupal\crocheteer\Plugin\Hook\Form\HookFormPluginManager;
use Drupal\crocheteer\Plugin\Hook\HookPluginInterface;
use Traversable;

/**
 * Class HookFormAlterPluginManager.
 *
 * @see \Drupal\crocheteer\Annotation\HookFormAlter
 * @see \Drupal\crocheteer\Plugin\Hook\Form\Alter\HookFormAlterPluginInterface
 * @see plugin_api
 */
final class HookFormAlterPluginManager extends HookFormPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    parent::__construct(
      $namespaces,
      $cacheBackend,
      $moduleHandler,
      $loggerChannelFactory,
      HookPluginInterface::class,
      HookFormAlter::class,
      'crocheteer_form_alter'
    );
  }

}
