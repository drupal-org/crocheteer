<?php

namespace Drupal\crocheteer\Plugin\Hook\Form;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookFormPlugin.
 *
 * Hook Form Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\Form\BaseFormEvent $event
 */
abstract class HookFormPlugin extends HookPlugin {

  /**
   * The form.
   *
   * @var array
   */
  protected $form;
  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;
  /**
   * The form id.
   *
   * @var string
   */
  protected $formId;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->form = $this->event->getForm();
    $this->formState = $this->event->getFormState();
    $this->formId = $this->event->getFormId();
  }

}
