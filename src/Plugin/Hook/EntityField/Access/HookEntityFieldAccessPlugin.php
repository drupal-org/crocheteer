<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityField\Access;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityFieldAccessPlugin.
 *
 * Hook Entity Field Access Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityField\EntityFieldAccessEvent $event
 */
abstract class HookEntityFieldAccessPlugin extends HookPlugin {

  /**
   * The operation.
   *
   * @var string
   */
  protected $operation;

  /**
   * The field definition.
   *
   * @var \Drupal\Core\Field\FieldDefinitionInterface
   */
  protected $fieldDefinition;

  /**
   * The account.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The field item list.
   *
   * @var \Drupal\Core\Field\FieldItemListInterface
   */
  protected $items;

  /**
   * The access result.
   *
   * @var \Drupal\Core\Access\AccessResultInterface
   */
  protected $accessResult;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->operation = $this->event->getOperation();
    $this->fieldDefinition = $this->event->getFieldDefinition();
    $this->account = $this->event->getAccount();
    $this->items = $this->event->getItems();
    $this->accessResult = $this->event->getAccessResult();
  }

}
