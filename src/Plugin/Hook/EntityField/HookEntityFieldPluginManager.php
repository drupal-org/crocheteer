<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityField;

use Drupal\crocheteer\Plugin\Hook\HookPluginManager;

/**
 * Class HookEntityFieldPluginManager.
 *
 * Base Plugin Manager class for all Hook Entity Field Plugin classes.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EventInterface $event
 */
abstract class HookEntityFieldPluginManager extends HookPluginManager {}
