<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityExtra\FieldInfo;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityExtraFieldInfoPlugin.
 *
 * Hook Entity Extra Field Info Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoEvent $event
 */
abstract class HookEntityExtraFieldInfoPlugin extends HookPlugin {

  /**
   * Display field info type constant.
   *
   * @var string
   */
  protected const TYPE_DISPLAY = 'display';

  /**
   * Display field info type constant.
   *
   * @var string
   */
  protected const TYPE_FORM = 'form';

  /**
   * Field info.
   *
   * @var array
   */
  protected $fieldInfo;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->fieldInfo = $this->event->getFieldInfo();
  }

  /**
   * Add field info for a form display.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $fieldName
   *   The field name.
   * @param array $info
   *   The field info.
   */
  protected function addDisplayFieldInfo($entityType, $bundle, $fieldName, array $info) : void {
    $this->addFieldInfo($entityType, $bundle, $fieldName, $info, self::TYPE_DISPLAY);
  }

  /**
   * Add field info for a form display.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $fieldName
   *   The field name.
   * @param array $info
   *   The field info.
   */
  protected function addFormFieldInfo($entityType, $bundle, $fieldName, array $info) : void {
    $this->addFieldInfo($entityType, $bundle, $fieldName, $info, self::TYPE_FORM);
  }

  /**
   * Add field info for a given type.
   *
   * @param string $entityType
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   * @param string $fieldName
   *   The field name.
   * @param array $info
   *   The field info.
   * @param string $type
   *   The type.
   */
  private function addFieldInfo($entityType, $bundle, $fieldName, array $info, $type) : void {
    $this->fieldInfo[$entityType][$bundle][$type][$fieldName] = $info;
  }

}
