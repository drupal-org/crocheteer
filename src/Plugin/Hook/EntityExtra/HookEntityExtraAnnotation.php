<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityExtra;

use Drupal\crocheteer\Plugin\Hook\HookAnnotation;

/**
 * Class HookEntityExtraAnnotation.
 *
 * Base class for all Hook Entity Extra Annotation classes.
 */
abstract class HookEntityExtraAnnotation extends HookAnnotation {}
