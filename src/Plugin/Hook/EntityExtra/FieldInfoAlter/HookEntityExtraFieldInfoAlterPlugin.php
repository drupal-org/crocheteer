<?php

namespace Drupal\crocheteer\Plugin\Hook\EntityExtra\FieldInfoAlter;

use Drupal\crocheteer\Plugin\Hook\HookPlugin;
use Drupal\hook_event_dispatcher\Event\EventInterface;

/**
 * Class HookEntityExtraFieldInfoAlterPlugin.
 *
 * Hook Entity Extra Field Info Alter Plugin class.
 *
 * @property-read \Drupal\hook_event_dispatcher\Event\EntityExtra\EntityExtraFieldInfoAlterEvent $event
 */
abstract class HookEntityExtraFieldInfoAlterPlugin extends HookPlugin {

  /**
   * Field info.
   *
   * @var array
   */
  protected $fieldInfo;

  /**
   * {@inheritDoc}
   */
  public function setup(EventInterface $event) : void {
    parent::setup($event);
    $this->fieldInfo = $this->event->getFieldInfo();
  }

}
