<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityUpdate.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Update\HookEntityUpdatePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Update\HookEntityUpdatePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityUpdate extends HookEntityAnnotation {}
