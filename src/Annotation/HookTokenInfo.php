<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Token\HookTokenAnnotation;

/**
 * Class HookTokenInfo.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Token\HookTokenAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Token\Info\HookTokenInfoPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Token\Info\HookTokenInfoPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookTokenInfo extends HookTokenAnnotation {}
