<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation;

/**
 * Class HookViewsDataAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Views\DataAlter\HookViewsDataAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\DataAlter\HookViewsDataAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookViewsDataAlter extends HookViewsAnnotation {}
