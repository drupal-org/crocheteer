<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Path\HookPathAnnotation;

/**
 * Class HookPathInsert.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Path\Insert\HookPathInsertPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Path\Insert\HookPathInsertPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookPathInsert extends HookPathAnnotation {}
