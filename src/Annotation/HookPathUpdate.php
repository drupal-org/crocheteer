<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Path\HookPathAnnotation;

/**
 * Class HookPathUpdate.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Path\Update\HookPathUpdatePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Path\Update\HookPathUpdatePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookPathUpdate extends HookPathAnnotation {}
