<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Js\HookJsAnnotation;

/**
 * Class HookJsAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Js\HookJsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Js\Alter\HookJsAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Js\Alter\HookJsAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookJsAlter extends HookJsAnnotation {}
