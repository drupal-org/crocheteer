<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Block\HookBlockAnnotation;

/**
 * Class HookBlockBuildAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Block\HookBlockAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Block\BuildAlter\HookBlockAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Block\BuildAlter\HookBlockAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookBlockBuildAlter extends HookBlockAnnotation {}
