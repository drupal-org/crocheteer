<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityDelete.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Delete\HookEntityDeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Delete\HookEntityDeletePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityDelete extends HookEntityAnnotation {}
