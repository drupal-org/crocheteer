<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\TemplatePreProcess\HookTemplatePreProcessAnnotation;

/**
 * Class HookTemplatePreProcessDefaultVariablesAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\TemplatePreProcess\HookTemplatePreProcessAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\TemplatePreProcess\DefaultVariablesAlter\HookTemplatePreProcessDefaultVariablesAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\TemplatePreProcess\DefaultVariablesAlter\HookTemplatePreProcessDefaultVariablesAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookTemplatePreProcessDefaultVariablesAlter extends HookTemplatePreProcessAnnotation {}
