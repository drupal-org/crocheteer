<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityPreDelete.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\PreDelete\HookEntityPreDeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\PreDelete\HookEntityPreDeletePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityPreDelete extends HookEntityAnnotation {}
