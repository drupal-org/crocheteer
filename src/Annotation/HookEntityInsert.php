<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityInsert.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Insert\HookEntityInsertPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Insert\HookEntityInsertPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityInsert extends HookEntityAnnotation {}
