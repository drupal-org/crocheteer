<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\User\HookUserAnnotation;

/**
 * Class HookUserFormatNameAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\User\FormatNameAlter\HookUserFormatNameAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\FormatNameAlter\HookUserFormatNameAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookUserFormatNameAlter extends HookUserAnnotation {}
