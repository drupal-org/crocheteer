<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityTypeAnnotation;

/**
 * Class HookEntityBaseFieldInfo.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\BaseFieldInfo\HookEntityBaseFieldInfoPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityType\BaseFieldInfo\HookEntityBaseFieldInfoPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityBaseFieldInfo extends HookEntityTypeAnnotation {}
