<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation;

/**
 * Class HookViewsPreBuild.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PreBuild\HookViewsPreBuildPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PreBuild\HookViewsPreBuildPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookViewsPreBuild extends HookViewsAnnotation {}
