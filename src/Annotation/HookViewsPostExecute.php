<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation;

/**
 * Class HookViewsPostExecute.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PostExecute\HookViewsPostExecutePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PostExecute\HookViewsPostExecutePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookViewsPostExecute extends HookViewsAnnotation {}
