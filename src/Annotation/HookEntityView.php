<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityView.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\View\HookEntityViewPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\View\HookEntityViewPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityView extends HookEntityAnnotation {}
