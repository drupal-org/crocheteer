<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Page\HookPageAnnotation;

/**
 * Class HookPageBottom.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Page\Bottom\HookPageBottomPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Page\Bottom\HookPageBottomPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookPageBottom extends HookPageAnnotation {}
