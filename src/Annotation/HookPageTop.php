<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Page\HookPageAnnotation;

/**
 * Class HookPageTop.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Page\HookPageAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Page\Top\HookPageTopPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Page\Top\HookPageTopPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookPageTop extends HookPageAnnotation {}
