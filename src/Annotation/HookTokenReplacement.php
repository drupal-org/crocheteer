<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Token\HookTokenAnnotation;

/**
 * Class HookTokenReplacement.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Token\HookTokenAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Token\Replacement\HookTokenReplacementPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Token\Replacement\HookTokenReplacementPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookTokenReplacement extends HookTokenAnnotation {}
