<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Theme\HookThemeAnnotation;

/**
 * Class HookTheme.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemeAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Theme\HookThemePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookTheme extends HookThemeAnnotation {}
