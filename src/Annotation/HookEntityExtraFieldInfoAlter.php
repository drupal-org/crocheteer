<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraAnnotation;

/**
 * Class HookEntityExtraFieldInfoAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\FieldInfo\HookEntityExtraFieldInfoAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\FieldInfo\HookEntityExtraFieldInfoAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityExtraFieldInfoAlter extends HookEntityExtraAnnotation {}
