<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Form\HookFormAnnotation;

/**
 * Class HookFormAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Form\HookFormAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Form\Alter\HookFormAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Form\Alter\HookFormAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookFormAlter extends HookFormAnnotation {}
