<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Path\HookPathAnnotation;

/**
 * Class HookPathDelete.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Path\HookPathAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Path\Delete\HookPathDeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Path\Delete\HookPathDeletePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookPathDelete extends HookPathAnnotation {}
