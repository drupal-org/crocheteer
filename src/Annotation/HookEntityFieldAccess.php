<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\EntityField\HookEntityFieldAnnotation;

/**
 * Class HookEntityFieldAccess.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\EntityField\HookEntityFieldAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\EntityField\Access\HookEntityFieldAccessPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityField\Access\HookEntityFieldAccessPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityFieldAccess extends HookEntityFieldAnnotation {}
