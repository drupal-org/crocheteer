<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityTranslationInsert.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\TranslationDelete\HookEntityTranslationInsertPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\TranslationDelete\HookEntityTranslationInsertPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityTranslationInsert extends HookEntityAnnotation {}
