<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Toolbar\HookToolbarAnnotation;

/**
 * Class HookToolbarAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Toolbar\HookToolbarAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Toolbar\Alter\HookToolbarAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Toolbar\Alter\HookToolbarAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookToolbarAlter extends HookToolbarAnnotation {}
