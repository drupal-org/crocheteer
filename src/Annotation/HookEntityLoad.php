<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityLoad.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Load\HookEntityLoadPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Load\HookEntityLoadPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityLoad extends HookEntityAnnotation {}
