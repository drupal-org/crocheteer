<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation;

/**
 * Class HookViewsPostBuild.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PostBuild\HookViewsPostBuildPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PostBuild\HookViewsPostBuildPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookViewsPostBuild extends HookViewsAnnotation {}
