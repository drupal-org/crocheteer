<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityAccess.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Access\HookEntityAccessPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Access\HookEntityAccessPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityAccess extends HookEntityAnnotation {}
