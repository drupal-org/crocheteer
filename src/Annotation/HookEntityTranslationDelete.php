<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityTranslationDelete.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\TranslationDelete\HookEntityTranslationDeletePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\TranslationDelete\HookEntityTranslationDeletePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityTranslationDelete extends HookEntityAnnotation {}
