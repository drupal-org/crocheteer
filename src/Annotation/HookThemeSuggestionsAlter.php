<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\ThemeSuggestions\HookThemeSuggestionsAnnotation;

/**
 * Class HookThemeSuggestionsAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\ThemeSuggestions\HookThemeSuggestionsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\ThemeSuggestions\Alter\HookThemeSuggestionsAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\ThemeSuggestions\Alter\HookThemeSuggestionsAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookThemeSuggestionsAlter extends HookThemeSuggestionsAnnotation {}
