<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Cron\HookCronAnnotation;

/**
 * Class HookCron.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Cron\HookCronAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Cron\HookCronPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Cron\HookCronPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookCron extends HookCronAnnotation {}
