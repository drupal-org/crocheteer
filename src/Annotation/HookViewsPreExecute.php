<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation;

/**
 * Class HookViewsPreExecute.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PreExecute\HookViewsPreExecutePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\PreExecute\HookViewsPreExecutePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookViewsPreExecute extends HookViewsAnnotation {}
