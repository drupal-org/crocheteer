<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\User\HookUserAnnotation;

/**
 * Class HookUserLogin.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\User\Login\HookUserLoginPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\Login\HookUserLoginPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookUserLogin extends HookUserAnnotation {}
