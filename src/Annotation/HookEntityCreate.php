<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityCreate.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Create\HookEntityCreatePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\Create\HookEntityCreatePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityCreate extends HookEntityAnnotation {}
