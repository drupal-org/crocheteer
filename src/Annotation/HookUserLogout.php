<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\User\HookUserAnnotation;

/**
 * Class HookUserLogout.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\User\HookUserAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\User\Logout\HookUserLogoutPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\User\Logout\HookUserLogoutPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookUserLogout extends HookUserAnnotation {}
