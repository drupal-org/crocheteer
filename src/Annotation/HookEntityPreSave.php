<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation;

/**
 * Class HookEntityPreSave.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\HookEntityAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\PreSave\HookEntityPreSavePlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Entity\PreSave\HookEntityPreSavePluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityPreSave extends HookEntityAnnotation {}
