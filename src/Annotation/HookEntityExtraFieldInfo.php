<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraAnnotation;

/**
 * Class HookEntityExtraFieldInfo.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\HookEntityExtraAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\FieldInfo\HookEntityExtraFieldInfoPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\EntityExtra\FieldInfo\HookEntityExtraFieldInfoPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookEntityExtraFieldInfo extends HookEntityExtraAnnotation {}
