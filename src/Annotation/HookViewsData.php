<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation;

/**
 * Class HookViewsData.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\Views\HookViewsAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\Views\Data\HookViewsDataPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\Views\Data\HookViewsDataPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookViewsData extends HookViewsAnnotation {}
