<?php

namespace Drupal\crocheteer\Annotation;

use Drupal\crocheteer\Plugin\Hook\WidgetForm\HookWidgetFormAnnotation;

/**
 * Class HookWidgetFormAlter.
 *
 * Plugin namespace: Plugin\crocheteer\Hook.
 *
 * @see \Drupal\crocheteer\Plugin\Hook\WidgetForm\HookWidgetFormAnnotation
 * @see \Drupal\crocheteer\Plugin\Hook\WidgetForm\Alter\HookWidgetFormAlterPlugin
 * @see \Drupal\crocheteer\Plugin\Hook\WidgetForm\Alter\HookWidgetFormAlterPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
final class HookWidgetFormAlter extends HookWidgetFormAnnotation {}
